

" Environment {

    " Identify platform {
        silent function! OSX()
            return has('macunix')
        endfunction
        silent function! LINUX()
            return has('unix') && !has('macunix') && !has('win32unix')
        endfunction
        silent function! WINDOWS()
            return  (has('win32') || has('win64'))
        endfunction
    " }

    " Basics {
        set nocompatible        " Must be first line
        if !WINDOWS()
            set shell=/bin/sh
        endif
    " }

    " Windows Compatible {
        " On Windows, also use '.vim' instead of 'vimfiles'; this makes synchronization
        " across (heterogeneous) systems easier.
        if WINDOWS()
          set runtimepath=$HOME/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,$HOME/.vim/after
        endif
    " }

    " Arrow Key Fix {
        " https://github.com/spf13/spf13-vim/issues/780
        if &term[:4] == "xterm" || &term[:5] == 'screen' || &term[:3] == 'rxvt'
            inoremap <silent> <C-[>OC <RIGHT>
        endif
    " }

" }

" Use before config if available {
    if filereadable(expand("~/.vim/vimrc.before"))
        source ~/.vim/vimrc.before
    endif
" }

" Use bundles config {
    if filereadable(expand("~/.vim/vimrc.bundles"))
        source ~/.vim/vimrc.bundles
    endif
" }




" General {

    set viminfo+=n$HOME/.vim/.viminfo " must behind set nocompatible

    " if !has('gui')
        "set term=$TERM          " Make arrow and other keys work
    " endif
    filetype plugin indent on   " Automatically detect file types.
    syntax on                   " Syntax highlighting
    set mouse=a                 " Automatically enable mouse usage
    set mousehide               " Hide the mouse cursor while typing
    scriptencoding utf-8


    " Most prefer to automatically switch to the current file directory when
    " a new buffer is opened; to prevent this behavior, add the following to
    " your .vimrc.before.local file:
    "   let g:spf13_no_autochdir = 1
    if !exists('g:spf13_no_autochdir')
        autocmd BufEnter * if bufname("") !~ "^\[A-Za-z0-9\]*://" | lcd %:p:h | endif
        " Always switch to the current file directory
    endif

    "set autowrite                       " Automatically write a file when leaving a modified buffer
    set autoread
    set shortmess+=filmnrxoOtT          " Abbrev. of messages (avoids 'hit enter')
    set viewoptions=folds,options,cursor,unix,slash " Better Unix / Windows compatibility
    set virtualedit=onemore             " Allow for cursor beyond last character
    set history=1000                    " Store a ton of history (default is 20)
    "set spell                           " Spell checking on
    set hidden                          " Allow buffer switching without saving
    set iskeyword-=.                    " '.' is an end of word designator
    set iskeyword-=#                    " '#' is an end of word designator
    set iskeyword-=-                    " '-' is an end of word designator

    " Instead of reverting the cursor to the last position in the buffer, we
    " set it to the first line when editing a git commit message
    au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

    " http://vim.wikia.com/wiki/Restore_cursor_to_file_position_in_previous_editing_session
    " Restore cursor to file position in previous editing session
    " To disable this, add the following to your .vimrc.before.local file:
    "   let g:spf13_no_restore_cursor = 1
    if !exists('g:spf13_no_restore_cursor')
        function! ResCur()
            if line("'\"") <= line("$")
                silent! normal! g`"
                return 1
            endif
        endfunction

        augroup resCur
            autocmd!
            autocmd BufWinEnter * call ResCur()
        augroup END
    endif

    autocmd WinLeave * :setlocal nocursorline
    autocmd WinEnter * :setlocal cursorline

    " Setting up the directories {
        set backup                  " Backups are nice ...
        if has('persistent_undo')
            set undofile                " So is persistent undo ...
            set undolevels=1000         " Maximum number of changes that can be undone
            set undoreload=10000        " Maximum number lines to save for undo on a buffer reload
        endif

        " To disable views add the following to your .vimrc.before.local file:
        "   let g:spf13_no_views = 1
        if !exists('g:spf13_no_views')
            " Add exclusions to mkview and loadview
            " eg: *.*, svn-commit.tmp
            let g:skipview_files = [
                \ '\[example pattern\]'
                \ ]
        endif
    " }



    " tag search
    set tags=./tags;$HOME

    " leave ex mode for good
    nnoremap Q <nop>

    " Switch Off The Current Search
    nnoremap <silent> <Leader><Leader>/ :nohlsearch<CR>

    command W w
" }


" Vim UI {


    set background=dark

"    set tabpagemax=15               " Only show 15 tabs
    set showmode                    " Display the current mode

    set cursorline                  " Highlight current line

    autocmd VimEnter,Colorscheme * :highlight clear SignColumn      " SignColumn should match background
    autocmd VimEnter,Colorscheme * :highlight clear LineNr          " Current line number row will have same background color in relative mode
"    autocmd VimEnter,Colorscheme * :highlight clear CursorLineNr    " Remove highlight color from current line number


    if has('cmdline_info')
        set ruler                   " Show the ruler
        set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
        set showcmd                 " Show partial commands in status line and
                                    " Selected characters/lines in visual mode
    endif

    if has('statusline')
        set laststatus=2

        " Broken down into easily includeable segments
        set statusline=%<%f\                     " Filename
        if isdirectory(expand("~/.vim/bundle/TagBar/"))
            set statusline+=%{tagbar#currenttag('[%s]','')}  " tagbar
        endif

        set statusline+=%w%h%m%r                 " Options
"        if !exists('g:override_spf13_bundles')
"            set statusline+=%{fugitive#statusline()} " Git Hotness
"        endif
        set statusline+=\ [%{&ff}/%Y]            " Filetype
        set statusline+=\ [%{getcwd()}]          " Current dir
        set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
    endif

    set backspace=indent,eol,start  " Backspace for dummies
    set linespace=0                 " No extra spaces between rows
    set number                      " Line numbers on
    set showmatch                   " Show matching brackets/parenthesis
    set incsearch                   " Find as you type search
    set hlsearch                    " Highlight search terms
"    set winminheight=0              " Windows can be 0 line high
    set ignorecase                  " Case insensitive search
    set smartcase                   " Case sensitive when uc present
    set wildmenu                    " Show list instead of just completing
    set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.
    set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
    set scrolljump=5                " Lines to scroll when cursor leaves screen
    set scrolloff=3                 " Minimum lines to keep above and below cursor
"    set foldenable                  " Auto fold code
    set list
    set listchars=tab:>\ ,trail:.,extends:#,precedes:#,nbsp:. " Highlight problematic whitespace
"    set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace

" }


" Formatting {

    set nowrap                      " Do not wrap long lines
    set autoindent                  " Indent at the same level of the previous line
    set shiftwidth=4                " Use indents of 4 spaces
    set expandtab                   " Tabs are spaces, not tabs
    set tabstop=4                   " An indentation every four columns
    set softtabstop=4               " Let backspace delete indent
    set nojoinspaces                " Prevents inserting two spaces after punctuation on a join (J)
    set splitright                  " Puts new vsplit windows to the right of the current
    set splitbelow                  " Puts new split windows to the bottom of the current
    set matchpairs+=<:>             " Match, to be used with %
    set pastetoggle=<F12>           " pastetoggle (sane indentation on pastes)
    "set comments=sl:/*,mb:*,elx:*/  " auto format comment blocks
    " Remove trailing whitespaces and ^M chars
    " To disable the stripping of whitespace, add the following to your
    " .vimrc.before.local file:
    "   let g:spf13_keep_trailing_whitespace = 1


    " preceding line best in a plugin but here for now.

"    autocmd FileType c,cpp,java,go,php,javascript,puppet,python,rust,twig,xml,yml,perl,sql autocmd BufWritePre <buffer> if !exists('g:spf13_keep_trailing_whitespace') | call StripTrailingWhitespace() | endif
"    autocmd FileType go autocmd BufWritePre <buffer> Fmt

    autocmd BufNewFile,BufRead *.html.twig set filetype=html.twig
    autocmd BufNewFile,BufRead *.coffee set filetype=coffee
    autocmd BufNewFile,BufRead *.h set filetype=c

    autocmd Filetype c,python setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=79 expandtab autoindent

    autocmd FileType haskell,puppet,ruby,yml setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2

    " Workaround vim-commentary for Haskell
    autocmd FileType haskell setlocal commentstring=--\ %s
    " Workaround broken colour highlighting in Haskell
    autocmd FileType haskell,rust setlocal nospell

    autocmd FileType c source ~/.vim/bundle/ifdef-highlighting/syntax/ifdef.vim
" }


" Session management {

    set sessionoptions=buffers

    " Automatically save the current session whenever vim is closed
    autocmd VimLeave * mksession! ~/.vim/session.shutdown.vim

    " <F7> restores that 'shutdown session'
    noremap <F7> :source ~/.vim/session.shutdown.vim<CR>

    " If you really want to, this next line should restore the shutdown session 
    " automatically, whenever you start vim.  (Commented out for now, in case 
    " somebody just copy/pastes this whole block)
    " 
    " autocmd VimEnter source ~/.vim/session.shutdown.vim<CR>

    " manually save a session with <F5>
    noremap <F5> :mksession! ~/.vim/session.manual.vim<cr>

    " recall the manually saved session with <F6>
    noremap <F6> :source ~/.vim/session.manual.vim<cr>

" }


" Plugins {


    " VimOrganizer {
""        if isdirectory(expand("~/.vim/bundle/VimOrganizer"))
""            au! BufRead,BufWrite,BufWritePost,BufNewFile *.org
""            au BufEnter *.org call org#SetOrgFileType()
""            let g:org_capture_file = '~/org/mycaptures.org'
""            command! OrgCapture :call org#CaptureBuffer()
""            command! OrgCaptureFile :call org#OpenCaptureFile()
""            let g:org_agenda_select_dirs=['~/org']
""            let g:agenda_files = split(glob("~/org/*.org"),'\n')
""            let g:org_todo_setup='TODO STARTED WAITING ASSIGNED ROUTINE | DONE CANCELLED'
""
""            function! Org_after_todo_state_change_hook(line,state1, state2)
""                    "call confirm("changed: ".a:line."--key:".a:state1." val:".a:state2)
""                    call OrgConfirmDrawer('LOGBOOK')
""                    let org#str = ': - State: ' . org#Pad(a:state2,10) . '   from: ' . org#Pad(a:state1,10) .
""                                \ '    [' . org#Timestamp() . ']'
""                    call append(line("."), repeat(' ',len(matchstr(getline(line(".")),'^\s*'))) . org#str)
""            endfunction
""        endif
    " }


    " PyMode {
        " Disable if python support not present
        if !has('python') && !has('python3')
            let g:pymode = 0
        endif

        if isdirectory(expand("~/.vim/bundle/python-mode"))
            let g:pymode_lint_checkers = ['pyflakes']
            let g:pymode_trim_whitespaces = 0
            let g:pymode_options = 0
            let g:pymode_rope = 0
        endif
    " }


    " TagBar {
        if isdirectory(expand("~/.vim/bundle/TagBar/"))
            nnoremap <Leader>tt :TagbarToggle<CR>
        endif
    "}

    " Rainbow {
        if isdirectory(expand("~/.vim/bundle/rainbow_parentheses.vim/"))
            let g:rbpt_colorpairs = [
                \ ['brown',       'RoyalBlue3'],
                \ ['lightblue',    'SeaGreen3'],
                \ ['lightgray',    'DarkOrchid3'],
                \ ['green',   'firebrick3'],
                \ ['cyan',    'RoyalBlue3'],
                \ ['red',     'SeaGreen3'],
                \ ['magenta', 'DarkOrchid3'],
                \ ['brown',       'firebrick3'],
                \ ['gray',        'RoyalBlue3'],
                \ ['white',       'SeaGreen3'],
                \ ['magenta', 'DarkOrchid3'],
                \ ['lightblue',    'firebrick3'],
                \ ['green',   'RoyalBlue3'],
                \ ['cyan',    'SeaGreen3'],
                \ ['yellow',     'DarkOrchid3'],
                \ ['red',         'firebrick3'],
                \ ]
            au VimEnter * RainbowParenthesesActivate
            au Syntax * RainbowParenthesesLoadRound
            au Syntax * RainbowParenthesesLoadSquare
            au Syntax * RainbowParenthesesLoadBraces
            au Syntax * RainbowParenthesesLoadChevrons
        endif
    "}


    " Normal Vim omni-completion {
    " To disable omni complete, add the following to your .vimrc.before.local file:
    "   let g:spf13_no_omni_complete = 1
            " Enable omni-completion.
            autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
            autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
            autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
            autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
            autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
            autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
            autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc

    " }

    " UndoTree {
        if isdirectory(expand("~/.vim/bundle/undotree/"))
            nnoremap <Leader>u :UndotreeToggle<CR>
            " If undotree is opened, it is likely one wants to interact with it.
            let g:undotree_SetFocusWhenToggle=1
        endif
    " }

    " indent_guides {
        if isdirectory(expand("~/.vim/bundle/vim-indent-guides/"))
            let g:indent_guides_auto_colors = 0
            let g:indent_guides_start_level = 2
            autocmd VimEnter,Colorscheme * :let g:indent_guides_guide_size = 1
            let g:indent_guides_enable_on_vim_startup = 0
            autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=lightblue ctermbg=3
            autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=lightgreen ctermbg=4
        endif
    " }

    " Syntastic {
        if isdirectory(expand("~/.vim/bundle/Syntastic/"))
            set statusline+=%#warningmsg#
            set statusline+=%{SyntasticStatuslineFlag()}
            set statusline+=%*

            let g:syntastic_always_populate_loc_list = 1
            let g:syntastic_auto_loc_list = 2
            let g:syntastic_check_on_open = 0
            let g:syntastic_check_on_wq = 1

        endif
    " }

    " ctrlp {
        if isdirectory(expand("~/.vim/bundle/ctrlp.vim/"))
            if executable('rg')
                let g:ctrlp_user_command = 'rg %s --files --color=never --glob ""'
                let g:ctrlp_use_caching = 0
            endif
            let g:ctrlp_cmd = 'CtrlPBuffer'
            let g:ctrlp_working_path_mode = 'ra'

            let g:ctrlp_custom_ignore = {
                \ 'dir':  '\.git$\|\.hg$\|\.svn$',
                \ 'file': '\.exe$\|\.so$\|\.dll$\|\.pyc$' }
        endif
    "}

    " MiniBufExpl {
        if isdirectory(expand("~/.vim/bundle/minibufexpl.vim/"))
            let g:miniBufExplorerAutoStart = 1
            let g:miniBufExplBRSplit = 0
            let g:miniBufExplMaxSize = 3
            let g:miniBufExplBuffersNeeded = 2
            let g:miniBufExplHideWhenDiff = 1
        endif
    " }

    " MiniBufExpl {
        if isdirectory(expand("~/.vim/bundle/quickfixsigns/"))
            let g:quickfixsigns_blacklist_buffer = '\(^\|[\/]\)\(__.*__\|NERD_tree_.*\|.*\.org\|-MiniBufExplorer-\|\[unite\] - .*\)$'
        endif
    " }

    " bbye {
        if isdirectory(expand("~/.vim/bundle/vim-bbye/"))
            nnoremap <Leader>q :Bdelete<CR>
        endif
    " }

    " easy-motion {
        if isdirectory(expand("~/.vim/bundle/vim-easymotion/"))
            " nmap <Leader><Leader>f <Plug>(easymotion-overwin-f)
            " nmap <Leader><Leader>w <Plug>(easymotion-overwin-w)
        endif
    " }

    " google calendar {
        if isdirectory(expand("~/.vim/bundle/calendar.vim/"))
            let g:calendar_google_calendar = 1
            let g:calendar_google_task = 1
            nnoremap <Leader>cal :Calendar<CR>
        endif
    " }

    " simplenote {
        if isdirectory(expand("~/.vim/bundle/simplenote.vim/"))
            let g:SimplenoteUsername = "jstien@gmail.com"
            let g:SimplenotePassword = "qwerty"
            nnoremap <Leader>sn :SimplenoteList<CR>
        endif
    " }

    " vim-ack {
        if isdirectory(expand("~/.vim/bundle/ack.vim/"))
            " let g:ackprg = 'ag --nogroup --nocolor --column'
            let g:ackprg = 'rg --vimgrep --no-heading'
            nnoremap <Leader>a :Ack!<Space>
        endif
    " }

    " vim-mark {
        if isdirectory(expand("~/.vim/bundle/vim-mark/"))
            let g:mwPalettes = {
                        \	'mypalette': [
                        \   { 'ctermbg':'Cyan',       'ctermfg':'Black', 'guibg':'#8CCBEA', 'guifg':'Black' },
                        \   { 'ctermbg':'Green',      'ctermfg':'Black', 'guibg':'#A4E57E', 'guifg':'Black' },
                        \   { 'ctermbg':'Yellow',     'ctermfg':'Black', 'guibg':'#FF8000', 'guifg':'Black' },
                        \   { 'ctermbg':'Red',        'ctermfg':'Black', 'guibg':'#FF7272', 'guifg':'Black' },
                        \   { 'ctermbg':'Magenta',    'ctermfg':'Black', 'guibg':'#FFB3FF', 'guifg':'Black' },
                        \   { 'ctermbg':'Blue',       'ctermfg':'Black', 'guibg':'#9999FF', 'guifg':'Black' },
                        \   { 'ctermbg':'DarkRed',    'ctermfg':'Black', 'guibg':'#800000', 'guifg':'Black' },
                        \   { 'ctermbg':'Grey',       'ctermfg':'Black', 'guibg':'#C8C8C8', 'guifg':'Black' },
                        \],
                        \	'original': [
                        \   { 'ctermbg':'Cyan',       'ctermfg':'Black', 'guibg':'#8CCBEA', 'guifg':'Black' },
                        \   { 'ctermbg':'Green',      'ctermfg':'Black', 'guibg':'#A4E57E', 'guifg':'Black' },
                        \   { 'ctermbg':'Yellow',     'ctermfg':'Black', 'guibg':'#FFDB72', 'guifg':'Black' },
                        \   { 'ctermbg':'Red',        'ctermfg':'Black', 'guibg':'#FF7272', 'guifg':'Black' },
                        \   { 'ctermbg':'Magenta',    'ctermfg':'Black', 'guibg':'#FFB3FF', 'guifg':'Black' },
                        \   { 'ctermbg':'Blue',       'ctermfg':'Black', 'guibg':'#9999FF', 'guifg':'Black' },
                        \],
                        \	'extended': function('mark#palettes#Extended'),
                        \	'maximum': function('mark#palettes#Maximum')
                        \}
            let g:mwDefaultHighlightingPalette = 'mypalette'
        endif
    " }
" }



" GUI Settings {

    " GVIM- (here instead of .gvimrc)
    if has('gui_running')
        color molokai                 " Load a colorscheme
        set guioptions-=T           " Remove the toolbar
        set lines=40                " 40 lines of text instead of 24
        set columns=120
        set listchars=tab:›\ ,trail:•,extends:⇉,precedes:⇇,nbsp:% " Highlight problematic whitespace
        set showbreak=↪

"        if !exists("g:spf13_no_big_font")
"            if LINUX() && has("gui_running")
"                set guifont=Andale\ Mono\ Regular\ 12,Menlo\ Regular\ 11,Consolas\ Regular\ 12,Courier\ New\ Regular\ 14
"            elseif OSX() && has("gui_running")
"                set guifont=Andale\ Mono\ Regular:h12,Menlo\ Regular:h11,Consolas\ Regular:h12,Courier\ New\ Regular:h14
"            elseif WINDOWS() && has("gui_running")
"                set guifont=Andale_Mono:h10,Menlo:h10,Consolas:h10,Courier_New:h10
"            endif
"        endif
    else
        if &term == 'xterm' || &term == 'screen'
            set t_Co=256            " Enable 256 colors to stop the CSApprox warning and make xterm vim shine
        endif
        "set term=builtin_ansi       " Make arrow and other keys work
        color molokai                 " Load a colorscheme
    endif

" }

" Functions {

    " Initialize directories {
    function! InitializeDirectories()
        let parent = $HOME . '/.vim'
        let prefix = 'vim'
        let dir_list = {
                    \ 'backup': 'backupdir',
                    \ 'views': 'viewdir',
                    \ 'swap': 'directory' }

        if has('persistent_undo')
            let dir_list['undo'] = 'undodir'
        endif

        " To specify a different directory in which to place the vimbackup,
        " vimviews, vimundo, and vimswap files/directories, add the following to
        " your .vimrc.before.local file:
        "   let g:spf13_consolidated_directory = <full path to desired directory>
        "   eg: let g:spf13_consolidated_directory = $HOME . '/.vim/'
        if exists('g:spf13_consolidated_directory')
            let common_dir = g:spf13_consolidated_directory . prefix
        else
            let common_dir = parent . '/.' . prefix
        endif

        for [dirname, settingname] in items(dir_list)
            let directory = common_dir . dirname . '/'
            if exists("*mkdir")
                if !isdirectory(directory)
                    call mkdir(directory)
                endif
            endif
            if !isdirectory(directory)
                echo "Warning: Unable to create backup directory: " . directory
                echo "Try: mkdir -p " . directory
            else
                let directory = substitute(directory, " ", "\\\\ ", "g")
                exec "set " . settingname . "=" . directory
            endif
        endfor
    endfunction

    call InitializeDirectories()
    " }



    " Strip whitespace {
    function! StripTrailingWhitespace()
        " Preparation: save last search, and cursor position.
        let _s=@/
        let l = line(".")
        let c = col(".")
        " do the business:
        %s/\s\+$//e
        " clean up: restore previous search history, and cursor position
        let @/=_s
        call cursor(l, c)
    endfunction

    nnoremap <Leader>stw : call StripTrailingWhitespace() <CR>
    " }


    " if executable('rg')
    "     set grepprg=rg\ --vimgrep
    "     set grepformat=%f:%l:%c:%m
    " elseif executable('sift')
    "     set grepprg=sift\ -nMs\ --no-color\ --binary-skip\ --column\ --no-group\ --git\ --follow
    "     set grepformat=%f:%l:%c:%m
    " elseif executable('ag')
    "     set grepprg=ag\ --vimgrep\ --ignore=\"**.min.js\"
    "     set grepformat=%f:%l:%c:%m,%f:%l:%m
    " elseif executable('ack')
    "     set grepprg=ack\ --nogroup\ --nocolor\ --ignore-case\ --column
    "     set grepformat=%f:%l:%c:%m,%f:%l:%m
    " endif

    noremap cp :let @+ = expand("%:p")<cr>

    " grep in buffers {
    function! MyGrepInBuffers()
        cex[]
        let pattern = expand("<cword>")
        exec "bufdo grepadd! " . pattern . " %"
    endfunction

    nnoremap <Leader>gb : call MyGrepInBuffers() <CR>
    " }

    " grep in files {
    nnoremap <Leader>gf : grep! -Ie "<cword>" -r %:p:h
    " }

    " select last paste in visual mode {
    nnoremap <expr> gb '`[' . strpart(getregtype(), 0, 1) . '`]'
    " }


    " highlight line {

    " It is possible to highlight the entire line permanently (mapped to key \l):
    nnoremap <silent> <Leader>l :exe "let m = matchadd('WildMenu','\\%" . line('.') . "l')"<CR>
    " Or one could only highlight the word underneath the cursor (mapped to key \w):
    nnoremap <silent> <Leader>w :exe "let m=matchadd('WildMenu','\\<\\w*\\%" . line(".") . "l\\%" . col(".") . "c\\w*\\>')"<CR>
    " To highlight the words contained in the virtual column (mapped to \c):
    nnoremap <silent> <Leader>c :exe "let m=matchadd('WildMenu','\\<\\w*\\%" . virtcol(".") . "v\\w*\\>')"<CR>
    " And finally, one can clear the permanent highlights (mapped to \Enter):
    nnoremap <silent> <Leader><CR> :call clearmatches()<CR>

    " }

" }
